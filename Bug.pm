#!/usr/bin/perl
#
#  Bug.pm
#
#  Copyright 2013 Mònica Ramírez Arceda <monica@debian.org>
#
#  This file is part of wnpp-inconsistencies-suggestions.
#
#  wnpp-inconsistencies-suggestions is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  wnpp-inconsistencies-suggestions is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with wnpp-inconsistencies-suggestions. If not, see <http://www.gnu.org/licenses/>.
#
#  This module implements the class Bug, that describes a bug in Debian BTS.

package Bug;
use strict;
use warnings;

## Constructor with bug number
sub new {
    my $class = shift;
    my $self = {};
    $self->{number} = shift;
    $self->{data} = Devscripts::Debbugs::status($self->{number});
    $self->{log} = undef;
    bless $self, $class;
    return $self;
}

# Get bug description: text after -- in title
sub get_description {
    my( $self ) = @_;
    my $bug_title = $self->get_title();
    $bug_title =~ /\w\W+[\w-]+(--)?\W*(.*)/;
    my $bug_description = $2;
    return $bug_description;
}

## Get the last body of log bug.
sub get_last_mail_body {
    my ( $self ) = @_;
    my $bug_log = $self->get_log();
    my $bug_log_length = @$bug_log;
    my $bug_log_last_body = $bug_log->[$bug_log_length -1]->{body};
    return $bug_log_last_body;
}

# Get bug log
sub get_log {
    my( $self ) = @_;
    $self->{log} = Devscripts::Debbugs::bug_log($self->{number});
    return $self->{log};
}

# Get bug number
sub get_number {
    my( $self ) = @_;
    return $self->{number};
}

# Get bug owner
sub get_owner {
    my( $self ) = @_;
    my $bug_number = $self->{number};
    my $bug_data = $self->{data};
    my $bug_owner = $bug_data->{$bug_number}->{owner};
    return $bug_owner;
}

# Get bug submitter
sub get_submitter {
    my( $self ) = @_;
    my $bug_number = $self->{number};
    my $bug_data = $self->{data};
    my $bug_submitter = $bug_data->{$bug_number}->{originator};
    return $bug_submitter;
}

# Get bug title
sub get_title {
    my( $self ) = @_;
    my $bug_number = $self->{number};
    my $bug_data = $self->{data};
    my $bug_title = $bug_data->{$bug_number}->{subject};
    return $bug_title;
}

# Find out if the bug is a wnpp bug
sub is_wnpp {
    my( $self ) = @_;
    my $bug_number = $self->{number};
    my $bug_data = $self->{data};
    my $bug_package = $bug_data->{$bug_number}->{package};
    return $bug_package eq "wnpp";
}

# Get bugs that this bug is merged with.
sub merged_with {
    my( $self ) = @_;
    my $bug_number = $self->{number};
    my $bug_data = $self->{data};
    my $mergedwith = $bug_data->{$bug_number}->{mergedwith};
    return $mergedwith;
}

1;
