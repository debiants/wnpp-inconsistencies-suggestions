#!/usr/bin/perl
#
#  wnpp-inconsistencies-suggestions.pl
#
#  Copyright 2012 Mònica Ramírez Arceda <monica@debian.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# This script gives you suggestions about what to do in each inconsistency
# shown at http://qa.debian.org/~bartm/wnpp-rfs-mentors/wnpp-inconsistencies.txt
# It does not automate anything, you must review all given results and then act
# properly.
#
# It outputs to STDOUT all inconsistencies followed by their suggestions. It
# also generates a bunch of mails and files located at wnpp-suggestions
# directory, ready to be sent or added.
#
# WARNING: In most cases, if someone else has fixed the inconsistency, this
# script does not notice that. Do not do the same thing twice! ;-)

use strict;
use warnings;
use LWP::Simple;
use Cwd;
use lib '/usr/share/devscripts';
use Devscripts::Debbugs;
use Term::ANSIColor;
use MIME::Words qw(:all);
use Encode;
use File::Path qw(remove_tree);

use Bug;
use Mentors;

sub error0($$);
sub error1($$);
sub error2($$);
sub error3($$$);
sub error4($$$);
sub error6($$$$$);
sub error7($$$$$);
sub error8($$$$$);
sub error9($$);
sub error10($$$$);
sub error11($$);
sub error12($);
sub error13($$);
sub error14($$$$$$);

sub strip_newlines($);

# Array with regular expressions that match errors
my @errors = (
    qr/bug (.*) has subject (.*)/,                                                    #0
    qr/bug (.*) has type (.*)/,                                                       #1
    qr/(.*) (.*) has owner/,                                                          #2
    qr/bug (.*) is (.*) for package (.*) already in Debian/,                          #3
    qr/bug (.*) is (.*) for package (.*) not in debian/,                              #4
    qr/(.*) (.*) is merged with bug (.*) but type of bug (.*) is unknown, ignoring/,  #5
    qr/package (.*): bugs (.*) (.*) and (.*) (.*) are not merged/,                    #6
    qr/package (.*): bugs (.*) and (.*) have different types: (.*) (.*)/,             #7
    qr/package (.*): bugs (.*) (.*) and (.*) (.*) have different owners/,             #8
    qr/(.*) (.*) has no owner/,                                                       #9
    qr/(ITA|ITP) missing for package (.*) with RFS (.*) with (.*) in title/,          #10
    qr/ITP missing for package (.*) with RFS (.*)/,                                   #11
    qr/ITP missing for package (.*) at mentors$/,                                     #12
    qr/ITP missing for package (.*) at mentors \(wnpp bugs: (.*)\)/,                  #13
    qr/merged bugs (.*) (.*) and (.*) (.*) are for different packages: (.*) (.*)/     #14
);

clean_done_file();

# Get the file with inconsistencies
my $url = 'http://qa.debian.org/~bartm/wnpp-rfs-mentors/wnpp-inconsistencies.txt';
# my $url = 'http://localhost/wnpp-inconsistencies.txt';
my $wnpp_inconsistencies = get($url) or die;

# Generate suggestions
# TODO: Decide where to locate suggested-mails directory
# Create the dir where suggestions will be written (and remove its content if exist)
my $pwd = cwd();
my $suggestions_dir = "$pwd/wnpp-suggestions";
remove_tree($suggestions_dir,{keep_root => 1});
mkdir $suggestions_dir;
# Open file with all commands to be sent to control@b.d.o
open OUTPUT, ">$suggestions_dir/control\@bugs.debian.org" or die;
for(split(/^/, $wnpp_inconsistencies)) {
    # Check if the suggestion of this error is already done
    open DONE, "done_suggestions" or die;
    my $done = 0;
    my @array_of_data = <DONE>;
    foreach my $line (@array_of_data){
        if ($line eq $_){
            $done = 1;
        }
    }
    close DONE;
    # If it is already done, skip it
    if($done){
        next;
    }
    print "\n";
    print color 'bold';
    print "$_";
    print color 'reset';
    if(/$errors[0]/){
        # qr/bug (.*) has subject (.*)/
        error0($1,$2);
    }
    elsif(/$errors[1]/){
        # qr/bug (.*) has type (.*)/
        error1($1,$2);
    }
    elsif(/$errors[2]/){
        # qr/(.*) (.*) has owner/
        error2($1,$2);
    }
    elsif(/$errors[3]/){
        # qr/bug (.*) is (.*) for package (.*) already in Debian/
        error3($1,$2,$3);
    }
    elsif(/$errors[4]/){
        # qr/bug (.*) is (.*) for package (.*) not in debian/
        error4($1,$2,$3);
    }
    elsif(/$errors[5]/){
        # qr/(.*) (.*) is merged with bug (.*) but type of bug (.*) is unknown,
        # ignoring/
        print "TODO: No suggestions yet\n";
    }
    elsif(/$errors[6]/){
        # qr/package (.*): bugs (.*) (.*) and (.*) (.*) are not merged/
        error6($1,$2,$3,$4,$5);
    }
    elsif(/$errors[7]/){
        # qr/package (.*): bugs (.*) and (.*) have different types: (.*) (.*)/
        error7($1,$2,$3,$4,$5);
    }
    elsif(/$errors[8]/){
        # qr/package (.*): bugs (.*) (.*) and (.*) (.*) have different owners/
        error8($1,$2,$3,$4,$5);
    }
    elsif(/$errors[9]/){
        # qr/(.*) (.*) has no owner/
        error9($1,$2);
    }
    elsif(/$errors[10]/){
        # qr/(ITA|ITP) missing for package (.*) with RFS (.*) with (.*) in
        # title/
        error10($1,$2,$3,$4);
    }
    elsif(/$errors[11]/){
        # qr/ITP missing for package (.*) with RFS (.*)/
        error11($1,$2);
    }
    elsif(/$errors[12]/){
        # qr/ITP missing for package (.*) at mentors$/
        error12($1)
    }
    elsif(/$errors[13]/){
        # qr/ITP missing for package (.*) at mentors \(wnpp bugs: (.*)\)/
        error13($1,$2);
    }
    elsif(/$errors[14]/){
        # qr/merged bugs (.*) (.*) and (.*) (.*)
        # are for different packages: (.*) (.*)/
        error14($1,$2,$3,$4,$5,$6);
    }
}
print "\n";
print OUTPUT "thanks\n";
close OUTPUT;

sub error0($$){
    # qr/bug (.*) has subject (.*)/
    # The bug has a wrong title
    my $bug_number = $_[0];
    my $bug_title = $_[1];
    # Guess bug_type, package_name and bug_description
    my $bug_type;
    my $package_name;
    my $bug_description;
    if ($bug_title =~ /.*(ITP|ITA|RFP|RFA|RFH|O)[ :].*/){ $bug_type = $1; }
    if ($bug_title =~ /\w+\W+([\w-]+)/){ $package_name = lc($1); }
    if ($bug_title =~ /\w\W+[\w-]+(--)?\W*(.*)/){ $bug_description = $2;}
    if ($bug_type and $package_name and $bug_description) {
         print_suggestions_in_control("retitle $bug_number $bug_type: $package_name -- $bug_description");
    } elsif (!$bug_type) {
        # TODO: improve this mail: the bug could be a RFA, O, ITA or RFH as well
        fill_template("messages/message00-0",
                      "$suggestions_dir/$bug_number-submitter\@bugs.debian.org",
                      $bug_number);
        print "Bug type is not clear. See, review and send ";
        print "$suggestions_dir/$bug_number-submitter\@bugs.debian.org\n";
    } else {
        print_suggestions_in_control("retitle $bug_number TODO $bug_title");
    }
    print_links($bug_number);
}

sub error1($$){
    # qr/bug (.*) has type (.*)/
    # The bug has a wrong type
    my $bug_number = $_[0];
    my $bug_type = $_[1];
    my $bug = new Bug($bug_number);
    my $bug_title = $bug->get_title();
    $bug_title =~ s/$bug_type/TODO/;
    print_suggestions_in_control("retitle $bug_number $bug_title");
    print_links($bug_number);
}

# TODO: migrate to Bug class
sub error2($$){
    # qr/(.*) (.*) has owner/
    # A bug has owner and it wouldn't have it
    # If it is merged with an ITP, don't remove the owner
    # If it is a RFP|O|RFA|RFP remove the owner
    # Otherwise, suggest to retitle
    my $wnpp_type = $_[0];
    my $bug_number = $_[1];
    my $bug_data = Devscripts::Debbugs::status($bug_number);
    my $merged_bug_number = $bug_data->{$bug_number}->{mergedwith};
    # TODO: it doesn't work if the bug is merged with more than one bug
    if($merged_bug_number){
        my $merged_bug_data = Devscripts::Debbugs::status($merged_bug_number);
        my $merged_bug_title = $merged_bug_data->{$merged_bug_number}->{subject};
        $merged_bug_title =~ /(.*)(:.*)/;
        my $merged_bug_type = $1;
        if($merged_bug_type =~ /ITP|ITA/){
            # do nothing, it's not needed to change the owner
            # the bug must be retitled, but it will be done by error #7
            print "It is merged with an $merged_bug_type, don't remove the owner.\n";
        } else {
            print_suggestions_in_control("noowner $bug_number");
        }
    } elsif($wnpp_type =~ /RFH|O|RFA|RFP/) {
        # RFH|O|RFA|RFP should not have owner
        print_suggestions_in_control("noowner $bug_number");
    } else {
        # Bug type may be wrong (IPT instead of ITP, or TIA instead of ITA...),
        # it must be retitled and remove the owner
        # or not depending on the fixed type...
        my $bug_title = $bug_data->{$bug_number}->{subject};
        print_suggestions_in_control("retitle $bug_number TODO $bug_title");
    }
    print_links($bug_number);
}

sub error3($$$){
    # qr/bug (.*) is (.*) for package (.*) already in Debian/
    my $bug_number = $_[0];
    my $bug_type = $_[1];
    my $package_name = $_[2];
    # Let user choose interactively
    # We have 2 possibilities: it's the same package and we must close the bug
    # or it's not the same package and we must suggest the package must be
    # retitled.

    # TODO: messages/message03-0 should be used to do this
    my $rename_message = "Hi,\n\n";
    $rename_message .= "There is already a package $package_name in Debian: \n";
    $rename_message .= "https://tracker.debian.org/$package_name\n\n";
    $rename_message .= "But it is not the same package that you are trying ";
    $rename_message .= "to package.\n";
    $rename_message .= "You should rename your package and retitle this bug.\n\n";
    $rename_message .= "Regards.\n";

    my $bug = new Bug($bug_number);
    my $bug_log_last_body = $bug->get_last_mail_body();
    my $rename_message_w_newlines = strip_newlines($rename_message);
    my $bug_log_last_body_w_newlines = strip_newlines($bug_log_last_body);
    if ($bug_log_last_body_w_newlines eq $rename_message_w_newlines) {
        print "Suggestion sent to owner. ";
        print "Wait for the owner to rename the package.\n";
    } else {
        print "If both packages are the same, see, review and send ";
        print "$suggestions_dir/$bug_number-close\@bugs.debian.org\n";
        print "If packages are not the same, see, review and send ";
        print "$suggestions_dir/$bug_number-submitter\@bugs.debian.org\n";

        fill_template("messages/message03-0",
                      "$suggestions_dir/$bug_number-submitter\@bugs.debian.org",
                      $package_name);

        fill_template("messages/message03-1",
                      "$suggestions_dir/$bug_number-close\@bugs.debian.org",
                      $package_name);
    }
    print_links($bug_number,"https://tracker.debian.org/$package_name");
}

sub error4($$$){
    # qr/bug (.*) is (.*) for package (.*) not in debian/
    # Probably the package has been removed, so orphaning it has no sense
    # Suggest to close the bug with a message giving the reason
    my $bug_number = $_[0];
    my $bug_type = $_[1];
    my $package_name = $_[2];
    if ($bug_type =~ /ITA|O|RFH|RFA/) {
        print "See, review and send ";
        print "$suggestions_dir/$bug_number-close\@bugs.debian.org\n";
        print_links($bug_number,"https://tracker.debian.org/$package_name");
        fill_template("messages/message04-0",
                      "$suggestions_dir/$bug_number-close\@bugs.debian.org",
                      $package_name);
    } else {
        # If the package has another type, this type will be a wrong type.
        # This issue is fixed in error1.
        print_suggestions_in_control();
        print_links($bug_number);
    }
}

sub error6($$$$$){
    # qr/package (.*): bugs (.*) (.*) and (.*) (.*) are not merged/
    # Two equal bugs are not merged
    # TODO: this line should be at the end of control mail
    # TODO: if one of the bugs is blocked, bugs can not be merged,
    #       we could do a forcemerge
    my $package_name = $_[0];
    my $bug1_type = $_[1];
    my $bug1_number = $_[2];
    my $bug2_type = $_[3];
    my $bug2_number = $_[4];
    print_suggestions_in_control("merge $bug1_number $bug2_number");
    print_links($bug1_number,$bug2_number);
}

sub error7($$$$$){
    # qr/package (.*): bugs (.*) and (.*) have different types: (.*) (.*)/
    # Two bugs of the same package have different types
    # Get bugs info
    my $package_name = $_[0];
    my $bug1_number = $_[1];
    my $bug2_number = $_[2];
    my $bug1_type = $_[3];
    my $bug2_type = $_[4];
    my $bug1 = new Bug($bug1_number);
    my $bug2 = new Bug($bug2_number);
    my $bug1_title = $bug1->get_title();
    my $bug1_desc = $bug1->get_description();
    my $bug2_title = $bug2->get_title();
    my $bug2_desc = $bug2->get_description();
    if($bug1_type eq $bug2_type){
        print "These two bugs already have the same type. ";
        print "Wait for wnpp inconsistencies to refresh.\n";
    } else {
        # Detect if bugs are merged.
        # TODO: it doesn't work if the bug is merged with more than one
        # bug.
        my $merged_bug_number = $bug1->merged_with();
        my $is_merged = $merged_bug_number eq $bug2_number;
        # If one of them is an ITP bug and the other one is a RFP,
        # suggest to retitle the RFP one.
        # If bugs are merged, BTS changes title to both merged bugs:
        # suggest to use the ITP title.
        my $suggestion;
        if(($bug1_type eq "RFP" && $bug2_type eq "ITP")){
            if($is_merged){
                $suggestion = "retitle $bug1_number ITP: $package_name -- $bug2_desc";
            } else {
                $suggestion = "retitle $bug1_number ITP: $package_name -- $bug1_desc";
            }
        } elsif($bug1_type eq "ITP" && $bug2_type eq "RFP"){
            if($is_merged){
                $suggestion = "retitle $bug2_number ITP: $package_name -- $bug1_desc";
            } else {
                $suggestion = "retitle $bug2_number ITP: $package_name -- $bug2_desc";
            }
        } else {
            print "TODO: No suggestions yet\n";
        }
        # If they are not merged, they will be merged in error6
        print_suggestions_in_control($suggestion);
        print_links($bug1_number,$bug2_number);
    }
}

sub error8($$$$$){
    # qr/package (.*): bugs (.*) (.*) and (.*) (.*) have different owners/
    # Two bugs of the same package have different owners
    my $package_name = $_[0];
    my $bug1_type = $_[1];
    my $bug1_number = $_[2];
    my $bug2_type = $_[3];
    my $bug2_number = $_[4];
    # The suggestion is to set the owner of the first ITP|ITA
    # as the owner of the second ITP|ITA.
    if($bug1_type =~ /ITP|ITA/) {
        my $bug1 = new Bug($bug1_number);
        my $owner1 = $bug1->get_owner();
        print_suggestions_in_control("owner $bug2_number $owner1");
        # It both bugs are ITP|ITA, suggest to write to the second bug to
        # inform the second owner and ask him/her to contact the first owner.
        if($bug1_type eq $bug2_type){
            fill_template("messages/message08-0",
                      "$suggestions_dir/$bug2_number-submitter\@bugs.debian.org",
                      $bug1_type, $bug1_number, $owner1);
            print " and $suggestions_dir/$bug2_number-submitter\@bugs.debian.org\n";
            # If first ITP seems to be abandoned, suggest second owner to contact
            # the first owner
            fill_template("messages/message08-1",
                      "$suggestions_dir/$bug2_number-submitter.itp-abandoned\@bugs.debian.org",
                      $bug1_type, $package_name, $bug1_number, $owner1);

            print " or $suggestions_dir/$bug2_number-submitter.itp-abandoned \@bugs.debian.org";
            print " if the first ITP seems abandoned\n";
        }
    } elsif($bug2_type =~ /ITP|ITA/) {
        # The first bug is not an ITP|ITA, so the owner must be the second one
        my $bug2 = new Bug($bug2_number);
        my $owner2 = $bug2->get_owner();
        print_suggestions_in_control("owner $bug1_number $owner2");
    } else {
        # More cases?
        print "TODO: No suggestions yet";
    }
    print_links($bug1_number,$bug2_number);
}

sub error9($$) {
    # qr/(.*) (.*) has no owner/
    # ITP|ITA has no owner
    my $bug_type = $_[0];
    my $bug_number = $_[1];
    my $bug = new Bug($bug_number);
    my $owner = $bug->get_owner();
    if($owner){
        print "This bug has already owner. Wait for wnpp inconsistencies ";
        print "to refresh.\n";
    } else {
        # Suggest the last person who made an ITP/ITA retitle is the new owner
        my $bug_log = $bug->get_log();
        my $bug_log_length = @$bug_log;
        for(my $i = $bug_log_length -1; $i >= 0; $i--) {
            # TODO: if this bug is merged with another one, the command owner
            # will appear twice in the suggestion.
            if( $bug_log->[$i]->{body} =~ /retitle\ +#?(\d+|-1)\ +IT[PA]:/ ){
                my $header = $bug_log->[$i]->{header};
                $header =~ /From: (.*)/;
                $owner = encode("utf8", decode_mimewords($1));
                last;
            }
        }
        # If no body retitled the ITP|ITA, suggest the submitter is the owner
        if(!$owner){
            $owner = $bug->get_submitter();
        }
        print_suggestions_in_control("owner $bug_number $owner");
        print_links($bug_number);
    }
}

sub error10($$$$){
    # qr/(ITA|ITP) missing for package (.*) with RFS (.*) with (.*) in title/
    # A RFS has been reported but its corresponding ITA/ITP doesn't exist,
    # although ITA|ITP is in title.
    # TODO: Maybe a RFP|RFA of this package exists,
    #       so the suggestion should be retitle it.
    my $missing_bug_type = $_[0];
    my $package_name = $_[1];
    my $bug_number = $_[2];
    my $type_title = $_[3];
    if ($missing_bug_type eq "ITP"){
        my $mentors = new Mentors($package_name);
        my $bug_closed = $mentors->get_closed_bug();
        if ( $bug_closed ne '' ) {
            # The package in mentors is closing a bug,
            my $bug = new Bug($bug_closed);
            if ($bug->is_wnpp()){
                my $type = $bug->get_title();
                $type =~ /(.*)(:.*)/;
                $type = $1;
                if($type eq "RFP"){
                    my $title = $bug->get_title();
                    $title =~ s/RFP:(.*)/ITP:$1/;
                    print_suggestions_in_control("retitle $bug_closed $title");
                    print_suggestions_in_control("owner $bug_closed ".
                                                  $mentors->get_uploader_name()." <".
                                                  $mentors->get_uploader_mail().">");
                } elsif($type eq "ITP"){
                    # Check if this ITP is owned by someone else
                    my $bug_owner = $bug->get_owner();
                    $bug_owner =~ /<(.*)>/;
                    my $bug_mail = $1;
                    my $mentors_mail = $mentors->get_uploader_mail();
                    if($bug_mail eq $mentors_mail){
                        # Suppose it is the corresponding ITP bug, but names don't match.
                        # The suggestion is to retitle the bug with the mentors package name
                        my $title = $bug->get_title();
                        $title =~ s/ITP: (.*) --/ITP: $package_name --/;
                        print_suggestions_in_control("retitle $bug_closed $title");
                    } else {
                        # Owners don't match
                        fill_template("messages/message12-0",
                          "$suggestions_dir/$package_name.mentors-comment",
                          $bug_closed, $bug_owner, $package_name);
                        print "Go to http://mentors.debian.net/package/$package_name ";
                        print "and add the following comment: ";
                        print "$suggestions_dir/$package_name.mentors-comment\n";
                    }
                }
            } else {
                print "This bug is not a wnpp one. No suggestions yet.\n";
            }
        } else {
            # TODO: find if a RFP bug exists for this package.
            # If exists, the suggestion should be retitling this RFP bug.
            print "See, review and send $suggestions_dir/$bug_number-submitter\@bugs.debian.org\n";
            fill_template("messages/message10-0",
                          "$suggestions_dir/$bug_number-submitter\@bugs.debian.org",
                          $package_name, $missing_bug_type, $bug_number);
        }
    } else { # $missing_bug_type eq "ITA"
        # TODO: find the corresponding RFA or O bug to put it
        #       instead of FIX_RFA_OR_O_BUG_NUMBER
        print "See, review and send $suggestions_dir/$bug_number-submitter\@bugs.debian.org\n";
        fill_template("messages/message10-1",
                      "$suggestions_dir/$bug_number-submitter\@bugs.debian.org",
                      $package_name, $missing_bug_type, $bug_number);
        # TODO: if there is no RFA or O bug, it means that: it is not an ITA or that
        # the packager is trying to adopt a package that is not orpahned
        # Possible mail (supposing that package is not maintained):
        # According to [0], tilda has not its corresponding ITA bug, despite $bug_number title.
        # You should get in contact with MIA Team <mia@qa.debian.org> to ask
        # them to orphan tilda. Then, you could adopt it with the correspondign ITA bug.
        # Thanks for your work!
        # [0] http://qa.debian.org/~bartm/wnpp-rfs-mentors/wnpp-inconsistencies.txt
    }
    print_links($bug_number,"http://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=both;include=subject%3A$package_name;package=wnpp");
}

# TODO: migrate to Bug class
sub error11($$) {
    # qr/ITP missing for package (.*) with RFS (.*)/
    # A RFS has been reported but its corresponding ITP doesn't exist
    my $package_name = $_[0];
    my $bug_number = $_[1];
    my $mentors = new Mentors($package_name);
    my $bug_closed = $mentors->get_closed_bug();
    if ( $bug_closed ne '' ) {
        # The package in mentors is closing a bug,
        my $bug = new Bug($bug_closed);
        if ($bug->is_wnpp()){
            my $type = $bug->get_title();
            $type =~ /(.*)(:.*)/;
            $type = $1;
            if($type eq "RFP"){
                my $title = $bug->get_title();
                $title =~ s/RFP:(.*)/ITP:$1/;
                print_suggestions_in_control("retitle $bug_closed $title");
                print_suggestions_in_control("owner $bug_closed ".
                                              $mentors->get_uploader_name()." <".
                                              $mentors->get_uploader_mail().">");
            } elsif($type eq "ITP"){
                # Check if this ITP is owned by someone else
                my $bug_owner = $bug->get_owner();
                $bug_owner =~ /<(.*)>/;
                my $bug_mail = $1;
                my $mentors_mail = $mentors->get_uploader_mail();
                if($bug_mail eq $mentors_mail){
                    # Suppose it is the corresponding ITP bug, but names don't match.
                    # The suggestion is to retitle the bug with the mentors package name
                    my $title = $bug->get_title();
                    $title =~ s/ITP: (.*) --/ITP: $package_name --/;
                    print_suggestions_in_control("retitle $bug_closed $title");
                } else {
                    # Owners don't match
                    fill_template("messages/message12-0",
                      "$suggestions_dir/$package_name.mentors-comment",
                      $bug_number, $bug_owner, $package_name);
                    print "Go to http://mentors.debian.net/package/$package_name ";
                    print "and add the following comment: ";
                    print "$suggestions_dir/$package_name.mentors-comment\n";
                }
            }
        } else {
            print "This bug is not a wnpp one. No suggestions yet.\n";
        }
    } else {
        print "See, review and send ";
        print "$suggestions_dir/$bug_number-submitter\@bugs.debian.org\n";
        fill_template("messages/message11-0",
                      "$suggestions_dir/$bug_number-submitter\@bugs.debian.org",
                      $package_name);
    }
    print_links($bug_number);
}

sub error12($) {
    # qr/ITP missing for package (.*) at mentors$/
    # A package has been uploaded to mentors
    # but there is not the corresponding ITP
    my $package_name = $_[0];
    my $mentors = new Mentors($package_name);
    my $bug_number = $mentors->get_closed_bug();
    if ( $bug_number ne '' ) {
        # The package in mentors is closing a bug,
        my $bug = new Bug($bug_number);
        if ($bug->is_wnpp()){
            # Check if this ITP is owned by someone else
            my $bug_owner = $bug->get_owner();
            $bug_owner =~ /<(.*)>/;
            my $bug_mail = $1;
            my $mentors_mail = $mentors->get_uploader_mail();
            if($bug_mail eq $mentors_mail){
                # Suppose it is the corresponding ITP bug, but names don't match.
                # The suggestion is to retitle the bug with the mentors package name
                my $title = $bug->get_title();
                $title =~ s/ITP: (.*) --/ITP: $package_name --/;
                print_suggestions_in_control("retitle $bug_number $title");
            } else {
                # Owners don't match
                fill_template("messages/message12-0",
                  "$suggestions_dir/$package_name.mentors-comment",
                  $bug_number, $bug_owner, $package_name);
                print "Go to http://mentors.debian.net/package/$package_name ";
                print "and add the following comment: ";
                print "$suggestions_dir/$package_name.mentors-comment\n";
            }
        } else {
            print "This bug is not a wnpp one. No suggestions yet.\n";
        }
        print_links("http://mentors.debian.net/package/$package_name",$bug_number);
    } else {
        # The package in mentors is not closing a bug, suggest to add a comment at mentors page
        print "Go to http://mentors.debian.net/package/$package_name ";
        print "and add the following comment: ";
        print "$suggestions_dir/$package_name.mentors-comment\n";
        print_links("http://mentors.debian.net/package/$package_name");
        fill_template("messages/message12-1",
                  "$suggestions_dir/$package_name.mentors-comment",
                  $package_name);
    }
}

sub error13($$) {
    # qr/ITP missing for package (.*) at mentors \(wnpp bugs: (.*)\)/
    # A package has been uploaded to mentors but there is not the corresponding ITP,
    # altough there is a wnpp bug related to this package in BTS
    # The suggestion is to convert the wnpp bug in BTS to an ITP bug and add
    # a comment to mentors
    my $package_name = $_[0];
    my $bug_number = $_[1];
    my $bug = new Bug($bug_number);

    # Suggest the new title
    my $bug_title = $bug->get_title();
    $bug_title =~ /(.*)(:.*)/;
    print_suggestions_in_control("retitle $bug_number ITP$2");

    # Suggest the uploader at mentors is the ITP owner
    my $mentors = new Mentors($package_name);
    my $uploader_name = $mentors->get_uploader_name();
    my $uploader_mail = $mentors->get_uploader_mail();
    print OUTPUT "owner $bug_number $uploader_name <$uploader_mail>\n";

    # Suggest owner to close the bug in the changelog file
    print "Go to http://mentors.debian.net/package/$package_name ";
    print "and add the following comment: ";
    print "$suggestions_dir/$package_name.mentors-comment\n";
    fill_template("messages/message13-0",
                  "$suggestions_dir/$package_name.mentors-comment",
                  $package_name, $bug_number);
    print_links($bug_number,"http://mentors.debian.net/package/$package_name");
}

sub error14($$$$$$) {
    # qr/merged bugs (.*) (.*) and (.*) (.*) are for different packages: (.*) (.*)/
    # If bugs are of the same type,
    # suggest to put the package name of the first bug to the second bug
    # If one of the bugs is an IT[PA] and the other one is a RFP,
    # suggest to put the title of the ITP to the RFP
    my $bug1_type = $_[0];
    my $bug1_number = $_[1];
    my $bug2_type = $_[2];
    my $bug2_number = $_[3];
    my $package1_name = $_[4];
    my $package2_name = $_[5];
    my $bug1 = new Bug($bug1_number);
    my $bug2 = new Bug($bug2_number);
    my $bug1_title = $bug1->get_title();
    my $bug2_title = $bug2->get_title();
    if($bug1_type =~ $bug2_type){
        $bug2_title =~ s/$package2_name/$package1_name/;
        print_suggestions_in_control("retitle $bug2_number $bug2_title");
    } elsif ($bug1_type =~ "IT[PA]" && $bug2_type =~ "RFP") {
        print_suggestions_in_control("retitle $bug2_number $bug1_title");
    } elsif ($bug1_type =~ "RFP" && $bug2_type =~ "IT[PA]") {
        print_suggestions_in_control("retitle $bug1_number $bug2_title");
    } else {
        print "TODO: No suggestions yet\n";
    }
    print_links($bug1_number, $bug2_number);
}

sub print_links(){
    # Print links related to an inconsistency
    print "Links: \n";
    foreach (@_) {
        # If the argument is a bug number, print the corresponding bug URL,
        # otherwise print the argument
        if ($_ =~ /^#?\d+$/) {
            print "http://bugs.debian.org/$_\n";
        } else {
            print "$_\n";
        }
    }
}

## Print a suggestion in control mail
sub print_suggestions_in_control(){
    my $suggestion = $_[0];
    print "See, review and send $suggestions_dir/control\@bugs.debian.org\n";
    # If there is no suggestion, it means that the issue has been fixed in
    # another inconsistency.
    if ($suggestion) {
        print OUTPUT "# $_";
        print OUTPUT "$suggestion\n";
    }
}

## Strip newlines of a string
sub strip_newlines($){
    my $str = $_[0];
    $str =~ s/\R//g;
    return $str;
}

## Replace placeholders in a template
sub fill_template {
    my $template = $_[0];
    my $message = $_[1];
    # my $word = $_[2];

    open TEMPLATE, $template or die;
    open MESSAGE,">$message" or die;

    while (<TEMPLATE>) {
        my $line = $_;
        for my $i (2 .. $#_) {
            my $template_index = $i - 2;
            $line =~ s/\$$template_index/$_[$i]/g;
        }
        print MESSAGE $line;
    }

    close TEMPLATE;
    close MESSAGE;
}

# Clean the file with done suggestions. Removes all the inconsistencies that
# do not appear in inconsistencies file, since they are fixed.
sub clean_done_file {
    # Get the file with inconsistencies
    my $url = 'http://qa.debian.org/~bartm/wnpp-rfs-mentors/wnpp-inconsistencies.txt';
    my $current_suggestions = get($url) or die;

    # Open the file with done suggestions and an auxiliar one where the fixed
    # ones will be removed
    open DONE, "done_suggestions" or die;
    open DONE_AUX, ">done_suggestions_aux" or die;
    my @array_of_data = <DONE>;

    # Traverse files and write those lines that are in both files
    foreach my $line (@array_of_data){
        for(split(/^/, $current_suggestions)) {
            if ($_ eq $line){
                print DONE_AUX $line;
                next;
            }
        }
    }

    close DONE;
    close DONE_AUX;

    rename "done_suggestions_aux", "done_suggestions";
}
