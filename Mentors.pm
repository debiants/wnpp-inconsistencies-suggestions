#  Copyright 2013 Mònica Ramírez Arceda <monica@debian.org>
#
#  This file is part of wnpp-inconsistencies-suggestions.
#
#  wnpp-inconsistencies-suggestions is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  wnpp-inconsistencies-suggestions is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with wnpp-inconsistencies-suggestions. If not, see <http://www.gnu.org/licenses/>.
#
#  This module implements the class Bug, that describes a bug in Debian BTS.

package Mentors;
use strict;
use warnings;
use DBI;

## Constructor with package name
sub new {
    my $class = shift;
    my $self = {};
    $self->{pkg_name} = shift;
    $self->{conn} = DBI->connect("dbi:Pg:dbname=udd; host=localhost;port=5452",
                                    "guest", "udd");
    my $pkg_id = $self->{conn}->prepare(
        "SELECT id
           FROM mentors_raw_packages
          WHERE name = '" . $self->{pkg_name} . "'");
    $pkg_id->execute();
    if($pkg_id->rows == 0){
        $self->{pkg_id} = -1;
    } else {
        $self->{pkg_id} = $pkg_id->fetchrow();
    }
    
    bless $self, $class;
    return $self;
}

## Destructor
sub DESTROY {
    my $self = shift;
    $self->{conn}->disconnect();
}

## Get the bug number that will be closed by this package
sub get_closed_bug {
    my ( $self ) = @_;
    if($self->{pkg_id} == -1){
        return '';
    }
    my $closes = $self->{conn}->prepare(
        "SELECT closes
           FROM mentors_most_recent_package_versions
          WHERE package_id = '" . $self->{pkg_id} . "'
          ORDER BY version DESC
          LIMIT 1");
    $closes->execute();
    my $closed_bug;
    if ($closed_bug = $closes->fetchrow()){
        return $closed_bug;
    } else {
        return '';
    }
}

## Get the uploader's name
sub get_uploader_name {
    my ( $self ) = @_;
    if($self->{pkg_id} == -1){
        return '';
    }
    my $uploader = $self->{conn}->prepare(
        "SELECT mentors_raw_users.name
           FROM mentors_raw_users
           JOIN mentors_raw_packages
             ON user_id = mentors_raw_users.id
          WHERE mentors_raw_packages.id = '" . $self->{pkg_id} . "'");
    $uploader->execute();
    return $uploader->fetchrow();
}

## Get the uploader's mail
sub get_uploader_mail {
    my ( $self ) = @_;
    if($self->{pkg_id} == -1){
        return '';
    }
    my $uploader = $self->{conn}->prepare(
        "SELECT email
           FROM mentors_raw_users
           JOIN mentors_raw_packages
             ON user_id = mentors_raw_users.id
          WHERE mentors_raw_packages.id = '" . $self->{pkg_id} . "'");
    $uploader->execute();
    return $uploader->fetchrow();
}

1;
